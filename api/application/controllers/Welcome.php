<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{

		$this->load->model('usermodel');
		$data['users']=$this->usermodel->getUsers();
//		$this->load->view('landing',$data);
		return json_encode($data['users']);

	}
	public function alluserdata()
	{
		$this->load->model('usermodel');
		$data['users']=$this->usermodel->getUsers();
		echo json_encode($data['users']);
	}
	public function singleuserdata()
	{
		$this->load->model('usermodel');
		$id = $this->input->post('id');
//		echo $data;
		$data['users']=$this->usermodel->getSingleUser($id);//
		echo json_encode($data['users']);
	}
	public function addnewuser()
	{
		$this->load->model('usermodel');
		$email = $this->input->post('email');
		$data['users']=$this->usermodel->addNewUser($email);//
		echo 1;
	}

}
