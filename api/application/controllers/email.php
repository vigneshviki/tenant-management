<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Email extends CI_Controller {

    public function __construct() {
        parent::__construct();

    }
// Show email page
    public function index() {


    }
    public function taskmail() {

        $this->load->model('emailmodel');
        $data['task']=$this->emailmodel->getlasttaskid();

        // Storing submitted values
        $sender_email = "vvsince91@gmail.com";
        $user_password = "gokugohan8";
        $receiver_email = $data['task'][0]->assigned_to;
        $username = "vignesh";
        $subject = "A new task has been assigned to you!";
        $message = '<html><body  bgcolor="#f6f6f6"><table class="body-wrap" bgcolor="#f6f6f6"><tr><td class="container" bgcolor="#FFFFFF" style="padding: 20px;border-radius: 10px;"><div class="content"><table><tr><td>';;
        $message .= '<h2> Hello! You have been assigned a new task</h2>';
        $message .= '<p>Please click the following button to view your task</p>';
        $message .= '<table class="btn-primary" cellpadding="0" cellspacing="0" border="0"><tr><td>';
        $message .= '<a href="localhost/ci/task.html?id='.$data['task'][0]->id.'"style="background-color: #348eda;border: solid 1px #348eda; border-radius: 25px;border-width: 10px 20px;display: inline-block;color: #ffffff;cursor: pointer;font-weight: bold;line-height: 2;text-decoration: none;">View My Task</a>';
        $message .= '</td></tr></table></td></tr></table></div></td></tr></table></body></html>';

// Configure email library
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = 465;
        $config['smtp_user'] = $sender_email;
        $config['smtp_pass'] = $user_password;
        $config['mailtype'] = 'html';

// Load email library and passing configured values to email library
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

// Sender email address
        $this->email->from($sender_email, $username);
// Receiver email address
        $this->email->to($receiver_email);
// Subject of email
        $this->email->subject($subject);
// Message in email
        $this->email->message($message);

        if ($this->email->send()) {
            echo json_encode("Email Successfully Send !");
        } else {
            show_error($this->email->print_debugger());
        }

    }
    public function notemail() {

        $this->load->model('emailmodel');
        $data['task']=$this->emailmodel->getlastnoteid();
        // Storing submitted values
        $sender_email = "vvsince91@gmail.com";
        $user_password = "gokugohan8";
        $receiver_email = $data['task'][0]->email;
        $username = "vignesh";
        $subject = "A new Note added !";
        $message = '<html><body  bgcolor="#f6f6f6"><table class="body-wrap" bgcolor="#f6f6f6"><tr><td class="container" bgcolor="#FFFFFF" style="padding: 20px;border-radius: 10px;"><div class="content"><table><tr><td>';;
        $message .= '<h2> Hello! A new note has been added to one of your tasks</h2>';
        $message .= '<p>Please click the following button to view your task</p>';
        $message .= '<table class="btn-primary" cellpadding="0" cellspacing="0" border="0"><tr><td>';
        $message .= '<a href="localhost/ci/task.html?id='.$data['task'][0]->task_id.'"style="background-color: #348eda;border: solid 1px #348eda; border-radius: 25px;border-width: 10px 20px;display: inline-block;color: #ffffff;cursor: pointer;font-weight: bold;line-height: 2;text-decoration: none;">View My Task</a>';
        $message .= '</td></tr></table></td></tr></table></div></td></tr></table></body></html>';

// Configure email library
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = 465;
        $config['smtp_user'] = $sender_email;
        $config['smtp_pass'] = $user_password;
        $config['mailtype'] = 'html';

// Load email library and passing configured values to email library
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

// Sender email address
        $this->email->from($sender_email, $username);
// Receiver email address
        $this->email->to($receiver_email);
// Subject of email
        $this->email->subject($subject);
// Message in email
        $this->email->message($message);

        if ($this->email->send()) {
            echo json_encode("Email Successfully Send !");
        } else {
            show_error($this->email->print_debugger());
        }

    }
}
?>