<?php

class Note extends CI_Controller{
    public function index(){

    }
    public function allnotes()
    {

        $this->load->model('notemodel');
        $id = $this->input->get('id');
        $range = $this->input->get('range');
        $data['users']=$this->notemodel->getnotes($id,$range);//
        echo json_encode($data['users']);
    }


    public function addnewnote()
    {
        $this->load->model('notemodel');
        $description = $this->input->post('description');
        $taskid = $this->input->post('taskid');
        $data['users']=$this->notemodel->postnote($description,$taskid);
        echo json_encode($data['users']);
    }


}