<?php

class Task extends CI_Controller{
    public function index(){

    }
    public function tasklist()
    {

        $this->load->model('taskmodel');
        $id = $this->input->get('id');
        $data['users']=$this->taskmodel->gettasklist($id);//
        echo json_encode($data['users']);
    }

    public function getsingletask()
    {
        $this->load->model('taskmodel');
        $id = $this->input->get('id');
        $data['users']=$this->taskmodel->getsingletask($id);
       echo json_encode($data['users']);
    }
    public function addnewtask()
    {
        $this->load->model('taskmodel');
        $description = $this->input->post('description');
        $due_date = $this->input->post('due_date');
        $assigned_to = $this->input->post('assigned_to');
        $assigned_by = $this->input->post('assigned_by');
//        $description = "asdf";
//        $due_date =  "asdf";
//        $assigned_to = "4";
//        $assigned_by =  "2";
        $data['users']=$this->taskmodel->addnewtask($description,$due_date,$assigned_to,$assigned_by);
        echo json_encode($data['users']);
    }


}