<?php


class UserModel extends CI_Model{

//    public $id;
//    public $email_id;
//
//    public function getid(){
//
//    }
//    public function getemail(){
//
//    }
//    public function setemail($e){
//        $this->email_id=$e;
//    }

    public function getUsers(){
        $this->load->database();
        $query = $this->db->query('SELECT * FROM user');
        return $query->result();
    }
    public function getSingleUser($id){
        $this->load->database();
        $sql = "SELECT * FROM user WHERE id = ?";
        $querySingleUser= $this->db->query($sql, array($id));
        return $querySingleUser->result();
}
    public function addNewUser($email){
        $this->load->database();
        $data = array(
            'email' => $email ,
        );
        $query=$this->db->insert('user', $data);
        return 1;
//        $showall = $this->db->query('SELECT * FROM user');
//        print_r($showall->result());
    }

}
