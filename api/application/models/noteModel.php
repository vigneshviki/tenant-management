<?php


class NoteModel extends CI_Model{

    public function getnotes($id,$range){
        $this->load->database();
        $sql2 = "SELECT * FROM note WHERE task_id = ?";
        $query2= $this->db->query($sql2, array($id));

        $sql = "SELECT * FROM note WHERE task_id = ? ORDER BY timestamp DESC LIMIT 5 OFFSET $range";
        $querySingleUser= $this->db->query($sql, array($id));



        $querySingleUser->result()[0]->size=sizeof($query2->result());
        return $querySingleUser->result();
    }

    public function postnote($description,$taskid){

        $this->load->database();
        $data = array(
            'description' => $description,
            'task_id' => $taskid ,
        );
        $query=$this->db->insert('note', $data);
        return 1;
    }

}

