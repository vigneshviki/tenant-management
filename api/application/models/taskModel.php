<?php


class TaskModel extends CI_Model{



    public function gettasklist($id){
        $this->load->database();
        $sql = "SELECT id,user_id FROM task WHERE user_id = ?";
        $querySingleUser= $this->db->query($sql, array($id));
        return $querySingleUser->result();
    }
    public function getsingletask($id){
        $this->load->database();
        $sql = "SELECT * FROM task WHERE id = ?";
        $querySingleUser= $this->db->query($sql, array($id));

        $sql2 = "SELECT * FROM user WHERE id = ?";
        $query2= $this->db->query($sql2, array($querySingleUser->result()[0]->user_id));

        // get the task and the corresponding user's email in a single object
        $querySingleUser->result()[0]->email=$query2->result()[0]->email;

        return $querySingleUser->result();
    }
    public function addnewtask($description,$due_date,$assigned_to,$assigned_by){

        $this->load->database();
        $data = array(
            'description' => $description,
            'due_date' => $due_date ,
            'assigned_to' => $assigned_to,
            'user_id' =>$assigned_by
        );
        $query=$this->db->insert('task', $data);
        return 1;
    }

}

